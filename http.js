var http = require('http');
var fs = require('fs');

http.createServer(function(request, response) {
  // The filename is simple the local directory and tacks on the requestuested url
  var filename = __dirname+request.url+'index.html';

  // This line opens the file as a readable stream
  var readStream = fs.createReadStream(filename);

  // This will wait until we know the readable stream is actually valid before piping
  readStream.on('open', function () {
    // This just pipes the read stream to the responseponse object (which goes to the client)
    readStream.pipe(response);
  });

  // This catches any errors that happen while creating the readable stream (usually invalid names)
  readStream.on('error', function(err) {
    response.end(err);
  });
}).listen(8080);

console.log('Server running at http://127.0.0.1:8080/');
