var EventEmitter = require('events').EventEmitter;
var events = new EventEmitter(); 

events.on('hello', function (name) {
    console.log('hello ' + name + '!');
});

events.emit('hello', 'world');
